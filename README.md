Academy Rental Group is Cincinnati's premier rental company for party rentals. We offer table rentals in greater Cincinnati for your party or event.

Address: 116 Marion Rd, Cincinnati, OH 45215, USA

Phone: 513-772-1929

Website: https://academyrentalsinc.com
